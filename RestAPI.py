#!flask/bin/python
from flask import Flask, jsonify,request
import time

from flask_httpauth import HTTPBasicAuth
auth = HTTPBasicAuth()


# def validate_json(xxx):
# def validate_schema(schema_name):
#     //and we add the bello 2 line after our route
#     @validate_json
#     @validate_schema('activate_schema')

timestamp = int(time.time())

app = Flask(__name__)

tasks = [
    {
        'id': 1,
        'ts': '1530228282',
        'sender': u'testy-test-service',
        'message': u'Milk, Cheese, Pizza, Fruit, Tylenol',
        'sent-from-ip': u'1.2.3.4',
        'priority': 2
    },
    {
        'id': 2,
        'ts': '1530228283',
        'sender': u'testy-test-service2',
        'message': u'Need to find a good Python tutorial on the web',
        'sent-from-ip': u'1.2.3.4',
        'priority': 2
    }
]

@auth.get_password
def get_password(username):
    if username == 'paul':
        return 'boicu'
    return None

@auth.error_handler
def unauthorized():
    return make_response(jsonify({'error': 'Unauthorized access'}), 403)

@app.route('/todo/api/v1.0/tasks', methods=['GET'])
@auth.login_required
def get_tasks():
    # return jsonify({'tasks': tasks})
    return jsonify({'tasks': tasks,'timp':timestamp, 'Marime':len(str(timestamp))})


@app.route('/todo/api/v1.0/tasks/<string:task_ts>', methods=['GET'])
def get_task(task_ts):
    task = [task for task in tasks if task['ts'] == task_ts]
    if len(task) == 0:
        abort(404)
    return jsonify({'task': task[0]})

@app.route('/todo/api/v1.0/tasks', methods=['POST'])
def create_task():
    task = {
        'id': tasks[-1]['id'] + 1,
        'ts': '1530228283',
        'sender': request.json['sender'],
        'message': request.json.get('message', ""),
        'sent-from-ip': request.json.get('sent-from-ip', ""),
        'priority': request.json['priority']
        }
    tasks.append(task)
    return jsonify({'task': task}), 201

if __name__ == '__main__':
    app.run(debug=True)