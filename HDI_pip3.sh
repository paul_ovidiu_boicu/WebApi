#!/bin/bash
apt-get install -y python3-pip
#pip3 install numpy pandas pyspark sklearn mysql-connector-python pytest pytest-cov pytest-pylint pytest-flake8
pip3 install numpy==1.14.3 pandas==0.24.0 pyspark==2.3.1 sklearn mysql-connector-python==8.0.15 pytest==4.3.1 pytest-cov==2.6.1 pytest-pylint==0.14.0 pytest-flake8==1.0.4 requests==2.21.0 pyodbc==4.0.26