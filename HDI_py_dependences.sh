#!/bin/bash
curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
curl https://packages.microsoft.com/config/ubuntu/16.04/prod.list > /etc/apt/sources.list.d/mssql-release.list
apt-get update
ACCEPT_EULA=Y apt-get -y install mssql-tools
apt-get -y install unixodbc-dev
sudo /usr/bin/anaconda/bin/conda install -y openssl=1.0.2o pymssql=2.1.3 sqlalchemy=1.2.7 pandas=0.22.0 tabulate=0.8.2 mysql-connector-python=2.0.4 pyodbc=4.0.23 astropy=2.0.7 automat=0.7.0 autovizwidget=0.12.5 constantly=15.1.0 freetds=1.00.9 h5py=2.7.0 incremental=17.5.0 matplotlib=2.0.2 intel-openmp=2018.0.3 libgfortran-ng=7.2.0 libstdcxx-ng=7.2.0 llvmlite=0.22.0 numexpr=2.6.5 pbr=4.0.4 prometheus_client=0.3.1 pywavelets=0.5.2 pyzmq=17.0.0 scikit-image=0.13.1 send2trash=1.5.0 subprocess32=3.5.2 terminado=0.8.1 twisted=17.1.0 unixodbc=2.3.6 zeromq=4.2.3 mkl-service=1.1.2 mkl_fft=1.0.1 ca-certificates=2018.03.07 libgcc-ng=7.2.0