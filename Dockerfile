FROM python:3.7
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY requirements.txt ./
COPY RestAPI.py ./
RUN pip install --no-cache-dir -r requirements.txt
COPY . .
#CMD [ "python", "./RestAPI.py"]
EXPOSE 5000
LABEL "RestAPI"="Alfav1"
LABEL version="0.1"
ENV FLASK_APP RestAPI.py
ENTRYPOINT ["python", "-m", "flask", "run", "--host=0.0.0.0"]