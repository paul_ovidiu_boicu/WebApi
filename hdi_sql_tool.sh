#!/bin/bash
curl https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
curl https://packages.microsoft.com/config/ubuntu/16.04/prod.list > /etc/apt/sources.list.d/mssql-release.list
apt-get update
apt-cache search mssql-server
ACCEPT_EULA=Y apt-get -y install mssql-tools