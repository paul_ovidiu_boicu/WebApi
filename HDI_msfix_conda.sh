#!/bin/bash
/bin/cat /usr/bin/anaconda/lib/python2.7/site-packages/conda/core/subdir_data.py |
/bin/sed 's/Executor = (DummyExecutor if context/#Executor = (DummyExecutor if context/' |
/bin/sed 's/else partial(ThreadLimitedThreadPoolExecutor,/#else partial(ThreadLimitedThreadPoolExecutor,/' |
/bin/sed 's/max_workers=context.repodata_threads))/#max_workers=context.repodata_threads))/' |
/bin/sed 's/with Executor() as executor:/with ThreadLimitedThreadPoolExecutor() as executor:/'  > /tmp/subdir_data.py.patched

sudo cp /tmp/subdir_data.py.patched /usr/bin/anaconda/lib/python2.7/site-packages/conda/core/subdir_data.py
sudo chmod 664 /usr/bin/anaconda/lib/python2.7/site-packages/conda/core/subdir_data.py