#!/bin/bash
# coverteste pki key (openssl)la keys (openssh) si creaza user
if [ `id -u $1 2>/dev/null || echo -1` -ge 0 ]; then  echo "user already exists"; exit 0; fi
useradd -s /bin/bash -g users -G docker,staff -m -p $(openssl rand -base64 32 | xargs openssl passwd -1) $1
mkdir /home/$1/.ssh
chmod 700 /home/$1/.ssh
chown $1:users /home/$1/.ssh
echo $2 | base64 --decode | openssl x509  -pubkey -noout > /home/$1/.ssh/$1.pub.tmp
ssh-keygen -f /home/$1/.ssh/$1.pub.tmp -i -m PKCS8 > /home/$1/.ssh/authorized_keys
chmod 400 /home/$1/.ssh/authorized_keys
chown $1:users /home/$1/.ssh/authorized_keys
chown -R $1 /home/$1/.ssh
chattr +i /home/$1/.ssh/authorized_keys
rm /home/$1/.ssh/$1.pub.tmp
